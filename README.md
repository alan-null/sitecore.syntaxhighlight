# Sitecore.SyntaxHighlight
Sitecore module for syntax highlighting  

## Installation
Please read my blog post. I explained how to install and use **Sitecore.SyntaxHighlight** module there. 

http://www.cognifide.com/blogs/sitecore/syntax-highlight-module-for-sitecore/ 


## Features
### SyntaxHighlight button:
Click to add a new code snippet or select the existing piece of code before, to load the code content into the editor.
new syntaxhighlight button in RichText editor
![alt t1ag](https://dl.dropboxusercontent.com/u/22467309/Sitecore/img/1-new-button-in-rt.jpg)

### Code editing window totally separated from Sitecore:
A new window is decoupled from Sitecore (pure HTML and JS, no XML nor server-side logic), thus it is easy to port it onto other Sitecore versions, with a few style fixes.
![alt t1ag](https://dl.dropboxusercontent.com/u/22467309/Sitecore/img/2-new-window.jpg)

### Advanced options:
You are able to highlight some lines, hide gutter (numbers) or start indexing from a different number (for example if yours shows a piece of code that is part of a bigger one).
![alt t1ag](https://dl.dropboxusercontent.com/u/22467309/Sitecore/img/3-advanced-option-tab.jpg)

### Different styling:
Fit the code snippets to your needs. Choose a different theme if your page design do not match the default one.
![alt t1ag](https://dl.dropboxusercontent.com/u/22467309/Sitecore/img/8-different-theme.jpg)